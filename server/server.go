package main

import (
	"log"
	"net/http"
	"os"
	"time"
	zapAuthGraphql "zapAuthGraphql"
	"zapAuthGraphql/directives"
	"zapAuthGraphql/resolvers"

	"github.com/99designs/gqlgen/handler"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"

	"github.com/rs/cors"
)

const pathApi = "/api/v1"
const pathAuth = pathApi + "/auth"

func main() {
	resolverHost()
	router := chi.NewRouter()

	serveCors := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		Debug:            false,
		MaxAge:           300,
	}).Handler

	router.Use(serveCors, AuthMiddleware, middleware.Timeout(time.Second*60))

	config := zapAuthGraphql.Config{Resolvers: &zapAuthGraphql.Resolver{}}
	config.Directives.HasAllowed = directives.HasAllowed

	router.Group(func(r chi.Router) {
		r.Get("/", handler.Playground("GraphQL playground", pathApi))
		r.Post(pathApi, handler.GraphQL(zapAuthGraphql.NewExecutableSchema(config)))
		r.Post(pathAuth, resolvers.AuthHandler())
	})

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", os.Getenv("PORT"))

	if err := http.ListenAndServe(":"+os.Getenv("PORT"), router); err != nil {
		log.Fatal(err)
	}
}

func AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var auth string
		var err error
		ctx := r.Context()
		auth = r.Header.Get("Authorization")
		if r.URL.Path != pathAuth && r.URL.Path != "/" {
			ctx, err = resolvers.VerifyToken(ctx, auth)

			if err != nil {
				w.Header().Set("Content-Type", "application/json; charset=UTF-8")
				http.Error(w, "{\"error\":\""+err.Error()+"\"}", 400)
				return
			}
			r.WithContext(ctx)

		}
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func resolverHost() {
	if os.Getenv("PORT") == "" {
		os.Setenv("PORT", "8080")
	}
	if os.Getenv("HOSTGRPCACCOUNT") == "" {
		os.Setenv("HOSTGRPCACCOUNT", "192.168.0.59:5050")
	}
	if os.Getenv("HOSTGRPCAUTH") == "" {
		os.Setenv("HOSTGRPCAUTH", "localhost:4040")
	}
}
