FROM golang:latest

ADD . /app

WORKDIR /app

CMD ["go","run","server/server.go"]