package directives

import (
	"context"
	"os"

	"github.com/99designs/gqlgen/graphql"
	common "gitlab.com/danieldefante/zap-common"
)

func HasAllowed(ctx context.Context, obj interface{}, next graphql.Resolver, permission *string) (interface{}, error) {
	if err := common.HasAllowed(ctx, obj, permission, os.Getenv("HOSTGRPCAUTH")); err != nil {
		return nil, err
	}

	return next(ctx)
}
