package resolvers

import (
	"context"
	"os"

	common "gitlab.com/danieldefante/zap-common"
	"gitlab.com/diogopazacvel/zap-auth-common-model/models"
	pb "gitlab.com/diogopazacvel/zap-auth-common-model/services"
)

func CreateApplication(ctx context.Context, applicationInput models.Application) (*models.CommonResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	var protoApplication *pb.Application = common.ModelToProto(&pb.Application{}, &applicationInput).(*pb.Application)

	retorno, err := clientGRPC.CreateApplication(ctx, protoApplication)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	applicationRetorno := common.ProtoToModel(&models.CommonResponse{}, retorno).(*models.CommonResponse)

	return applicationRetorno, nil
}

func UpdateApplication(ctx context.Context, id string, applicationInput models.Application) (*models.CommonResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	applicationInput.ID = id
	var protoApplication *pb.Application = common.ModelToProto(&pb.Application{}, &applicationInput).(*pb.Application)

	applicationProto, err := clientGRPC.UpdateApplication(ctx, protoApplication)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	applicationRetorno := common.ProtoToModel(&models.CommonResponse{}, applicationProto).(*models.CommonResponse)

	return applicationRetorno, nil
}

func DeleteApplication(ctx context.Context, id string) (*models.CommonResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	protoInputId := &pb.InputId{ID: id}

	respost, err := clientGRPC.DeleteApplication(ctx, protoInputId)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	return &models.CommonResponse{Message: respost.Message}, nil
}

func GetApplication(ctx context.Context, applicationInput models.Application) (*models.Application, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	var params *pb.Application = common.ModelToProto(&pb.Application{}, &applicationInput).(*pb.Application)

	protoApplication, err := clientGRPC.GetApplication(ctx, params)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	modelApplication := common.ProtoToModel(&models.Application{}, protoApplication).(*models.Application)

	return modelApplication, nil
}

func PagedApplication(ctx context.Context, inputPaged models.InputPaged) (*models.ApplicationPaged, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	offset := inputPaged.Offset
	limit := inputPaged.Limit

	retorno, err := clientGRPC.PagedApplication(ctx, &pb.InputPaged{Limit: int64(limit), Offset: int64(offset), Filters: inputPaged.Filters, OrderBy: inputPaged.OrderBy})
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	items := []models.Application{}
	for _, element := range retorno.Items {
		var applicationModel *models.Application = common.ProtoToModel(&models.Application{}, element).(*models.Application)
		items = append(items, *applicationModel)
	}

	return &models.ApplicationPaged{Size: int(retorno.Size), Offset: offset, Limit: limit, Items: items}, nil
}
