package resolvers

import (
	"context"
	"os"

	common "gitlab.com/danieldefante/zap-common"
	"gitlab.com/diogopazacvel/zap-auth-common-model/models"
	pb "gitlab.com/diogopazacvel/zap-auth-common-model/services"
)

func CreateRole(ctx context.Context, roleInput models.Role) (*models.CommonResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	var protoRole *pb.Role = common.ModelToProto(&pb.Role{}, &roleInput).(*pb.Role)

	retorno, err := clientGRPC.CreateRole(ctx, protoRole)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	roleRetorno := common.ProtoToModel(&models.CommonResponse{}, retorno).(*models.CommonResponse)

	return roleRetorno, nil
}

func UpdateRole(ctx context.Context, id string, roleInput models.Role) (*models.CommonResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	roleInput.ID = id
	var protoRole *pb.Role = common.ModelToProto(&pb.Role{}, &roleInput).(*pb.Role)

	roleProto, err := clientGRPC.UpdateRole(ctx, protoRole)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	roleRetorno := common.ProtoToModel(&models.CommonResponse{}, roleProto).(*models.CommonResponse)

	return roleRetorno, nil
}

func DeleteRole(ctx context.Context, id string) (*models.CommonResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	protoInputId := &pb.InputId{ID: id}

	respost, err := clientGRPC.DeleteRole(ctx, protoInputId)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	return &models.CommonResponse{Message: respost.Message}, nil
}

func GetRole(ctx context.Context, roleInput models.Role) (*models.Role, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	var params *pb.Role = common.ModelToProto(&pb.Role{}, &roleInput).(*pb.Role)

	protoRole, err := clientGRPC.GetRole(ctx, params)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	modelRole := common.ProtoToModel(&models.Role{}, protoRole).(*models.Role)

	return modelRole, nil
}

func PagedRole(ctx context.Context, applicationId string, inputPaged models.InputPaged) (*models.RolePaged, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	offset := inputPaged.Offset
	limit := inputPaged.Limit

	retorno, err := clientGRPC.PagedRole(ctx, &pb.InputPaged{Limit: int64(limit), Offset: int64(offset), Filters: inputPaged.Filters, OrderBy: inputPaged.OrderBy})
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	items := []models.Role{}
	for _, element := range retorno.Items {
		var roleModel *models.Role = common.ProtoToModel(&models.Role{}, element).(*models.Role)
		items = append(items, *roleModel)
	}

	return &models.RolePaged{Size: int(retorno.Size), Offset: offset, Limit: limit, Items: items}, nil
}
