package resolvers

import (
	"context"
	"os"

	common "gitlab.com/danieldefante/zap-common"
	"gitlab.com/diogopazacvel/zap-auth-common-model/models"
	pb "gitlab.com/diogopazacvel/zap-auth-common-model/services"
)

func CreateModule(ctx context.Context, moduleInput models.Module) (*models.CommonResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	var protoModule *pb.Module = common.ModelToProto(&pb.Module{}, &moduleInput).(*pb.Module)

	retorno, err := clientGRPC.CreateModule(ctx, protoModule)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	moduleRetorno := common.ProtoToModel(&models.CommonResponse{}, retorno).(*models.CommonResponse)

	return moduleRetorno, nil
}

func UpdateModule(ctx context.Context, id string, moduleInput models.Module) (*models.CommonResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	moduleInput.ID = id
	var protoModule *pb.Module = common.ModelToProto(&pb.Module{}, &moduleInput).(*pb.Module)

	moduleProto, err := clientGRPC.UpdateModule(ctx, protoModule)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	moduleRetorno := common.ProtoToModel(&models.CommonResponse{}, moduleProto).(*models.CommonResponse)

	return moduleRetorno, nil
}

func DeleteModule(ctx context.Context, id string) (*models.CommonResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	protoInputId := &pb.InputId{ID: id}

	respost, err := clientGRPC.DeleteModule(ctx, protoInputId)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	return &models.CommonResponse{Message: respost.Message}, nil
}

func GetModule(ctx context.Context, moduleInput models.Module) (*models.Module, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	var params *pb.Module = common.ModelToProto(&pb.Module{}, &moduleInput).(*pb.Module)

	protoModule, err := clientGRPC.GetModule(ctx, params)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	modelModule := common.ProtoToModel(&models.Module{}, protoModule).(*models.Module)

	return modelModule, nil
}

func PagedModule(ctx context.Context, applicationId string, inputPaged models.InputPaged) (*models.ModulePaged, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	offset := inputPaged.Offset
	limit := inputPaged.Limit

	retorno, err := clientGRPC.PagedModule(ctx, &pb.InputPaged{
		Limit:   int64(limit),
		Offset:  int64(offset),
		Filters: inputPaged.Filters,
		Params:  "{\"application_id\":\"" + applicationId + "\"}",
		OrderBy: inputPaged.OrderBy,
	})
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	items := []models.Module{}
	for _, element := range retorno.Items {
		var moduleModel *models.Module = common.ProtoToModel(&models.Module{}, element).(*models.Module)
		items = append(items, *moduleModel)
	}

	return &models.ModulePaged{Size: int(retorno.Size), Offset: offset, Limit: limit, Items: items}, nil
}

func GetAllModule(ctx context.Context, applicationId string) ([]*models.Module, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	retorno, err := clientGRPC.GetAllModule(ctx, &pb.InputId{
		ID: applicationId,
	})
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	items := []*models.Module{}
	for _, element := range retorno.Items {
		var moduleModel *models.Module = common.ProtoToModel(&models.Module{}, element).(*models.Module)
		items = append(items, moduleModel)
	}

	return items, nil
}
