package resolvers

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"os"
	"strings"

	common "gitlab.com/danieldefante/zap-common"
	"gitlab.com/diogopazacvel/zap-auth-common-model/models"

	pb "gitlab.com/diogopazacvel/zap-auth-common-model/services"
)

func AuthHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		ip := r.RemoteAddr
		var login pb.Login
		if r.Body == nil {
			http.Error(w, "Error required data", 400)
			return
		}
		err := json.NewDecoder(r.Body).Decode(&login)
		if err != nil {
			http.Error(w, err.Error(), 400)
			return
		}
		conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
		clientGRPC := pb.NewZapAuthServiceClient(conn)
		protoLogin := &pb.Login{
			Email:         login.Email,
			Password:      login.Password,
			Ip:            ip,
			ApplicationID: login.ApplicationID,
		}
		responseProtoLogin, err := clientGRPC.Login(ctx, protoLogin)
		common.CloseConn(conn)
		if err != nil {
			http.Error(w, err.Error(), 400)
			return
		}
		var response *models.LoginResponse = common.ProtoToModel(&models.LoginResponse{}, responseProtoLogin).(*models.LoginResponse)
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(200)
		json.NewEncoder(w).Encode(response.Token)
	}
}

func VerifyToken(ctx context.Context, token string) (context.Context, error) {
	if token == "" {
		return nil, errors.New("Token is required")
	}
	finalToken := strings.Replace(token, "Bearer ", "", -1)
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)
	protoToken := &pb.Token{
		Token: finalToken,
	}
	claims, err := clientGRPC.VerifyToken(ctx, protoToken)
	common.CloseConn(conn)
	if err != nil {
		return ctx, err
	}
	ctx = context.WithValue(ctx, "currentAccountId", claims.AccountID)
	ctx = context.WithValue(ctx, "applicationId", claims.ApplicationID)

	return ctx, nil
}

func RenewToken(ctx context.Context, token string) (*models.LoginResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	protoToken := &pb.Token{Token: token}

	respost, err := clientGRPC.RenewToken(ctx, protoToken)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	modelLoginResponse := common.ProtoToModel(&models.LoginResponse{}, respost).(*models.LoginResponse)

	return modelLoginResponse, nil
}

func RecoverPassword(ctx context.Context, email string) (*models.CommonResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAccountServiceClient(conn)
	protoAccount := &pb.Account{
		Email: email,
	}
	responseGrpc, err := clientGRPC.RecoverPassword(ctx, protoAccount)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}
	var response *models.CommonResponse = common.ProtoToModel(&models.CommonResponse{}, responseGrpc).(*models.CommonResponse)

	return response, nil
}
