package resolvers

import (
	"context"
	"os"

	common "gitlab.com/danieldefante/zap-common"
	"gitlab.com/diogopazacvel/zap-auth-common-model/models"
	pb "gitlab.com/diogopazacvel/zap-auth-common-model/services"
)

func CreatePermission(ctx context.Context, permissionInput models.Permission) (*models.CommonResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	var protoPermission *pb.Permission = common.ModelToProto(&pb.Permission{}, &permissionInput).(*pb.Permission)

	retorno, err := clientGRPC.CreatePermission(ctx, protoPermission)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	permissionRetorno := common.ProtoToModel(&models.CommonResponse{}, retorno).(*models.CommonResponse)

	return permissionRetorno, nil
}

func UpdatePermission(ctx context.Context, id string, permissionInput models.Permission) (*models.CommonResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	permissionInput.ID = id
	var protoPermission *pb.Permission = common.ModelToProto(&pb.Permission{}, &permissionInput).(*pb.Permission)

	permissionProto, err := clientGRPC.UpdatePermission(ctx, protoPermission)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	permissionRetorno := common.ProtoToModel(&models.CommonResponse{}, permissionProto).(*models.CommonResponse)

	return permissionRetorno, nil
}

func DeletePermission(ctx context.Context, id string) (*models.CommonResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	protoInputId := &pb.InputId{ID: id}

	respost, err := clientGRPC.DeletePermission(ctx, protoInputId)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	return &models.CommonResponse{Message: respost.Message}, nil
}

func GetPermission(ctx context.Context, permissionInput models.Permission) (*models.Permission, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	var params *pb.Permission = common.ModelToProto(&pb.Permission{}, &permissionInput).(*pb.Permission)

	protoPermission, err := clientGRPC.GetPermission(ctx, params)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	modelPermission := common.ProtoToModel(&models.Permission{}, protoPermission).(*models.Permission)

	return modelPermission, nil
}

func PagedPermission(ctx context.Context, moduleId string, inputPaged models.InputPaged) (*models.PermissionPaged, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	offset := inputPaged.Offset
	limit := inputPaged.Limit

	retorno, err := clientGRPC.PagedPermission(ctx, &pb.InputPaged{
		Limit:   int64(limit),
		Offset:  int64(offset),
		Filters: inputPaged.Filters,
		Params:  "{\"module_id\":\"" + moduleId + "\"}",
		OrderBy: inputPaged.OrderBy,
	})
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	items := []models.Permission{}
	for _, element := range retorno.Items {
		var permissionModel *models.Permission = common.ProtoToModel(&models.Permission{}, element).(*models.Permission)
		items = append(items, *permissionModel)
	}

	return &models.PermissionPaged{Size: int(retorno.Size), Offset: offset, Limit: limit, Items: items}, nil
}
