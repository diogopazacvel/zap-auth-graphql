package resolvers

import (
	"context"
	"os"

	common "gitlab.com/danieldefante/zap-common"
	"gitlab.com/diogopazacvel/zap-auth-common-model/models"
	pb "gitlab.com/diogopazacvel/zap-auth-common-model/services"
)

func CreateAccountRole(ctx context.Context, accountId string, roleId string) (*models.CommonResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	protoAccountRole := &pb.AccountRole{AccountID: accountId, RoleID: roleId}

	retorno, err := clientGRPC.CreateAccountRole(ctx, protoAccountRole)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	accountRoleRetorno := common.ProtoToModel(&models.CommonResponse{}, retorno).(*models.CommonResponse)

	return accountRoleRetorno, nil
}

func DeleteAccountRole(ctx context.Context, accountId string, roleId string) (*models.CommonResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	protoAccountRole := &pb.AccountRole{AccountID: accountId, RoleID: roleId}

	respost, err := clientGRPC.DeleteAccountRole(ctx, protoAccountRole)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	return &models.CommonResponse{Message: respost.Message}, nil
}

func GetAllAccountRoleByIDAccount(ctx context.Context, applicationId string, accountId string) ([]*models.AccountRoleVO, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	protoInputAccountRole := &pb.InputAccountRole{AccountID: accountId, ApplicationID: applicationId}

	accountRoleVOListProto, err := clientGRPC.GetAllAccountRoleByIDAccount(ctx, protoInputAccountRole)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	items := []*models.AccountRoleVO{}
	for _, element := range accountRoleVOListProto.Items {
		var accountRoleVOModel *models.AccountRoleVO = common.ProtoToModel(&models.AccountRoleVO{}, element).(*models.AccountRoleVO)
		items = append(items, accountRoleVOModel)
	}

	return items, nil
}
