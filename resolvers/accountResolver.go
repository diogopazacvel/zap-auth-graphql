package resolvers

import (
	"context"
	"os"

	common "gitlab.com/danieldefante/zap-common"
	"gitlab.com/diogopazacvel/zap-auth-common-model/models"
	pb "gitlab.com/diogopazacvel/zap-auth-common-model/services"
)

func CreateAccount(ctx context.Context, accountInput models.Account) (*models.CommonResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCACCOUNT"))
	clientGRPC := pb.NewZapAccountServiceClient(conn)

	var protoAccount *pb.Account = common.ModelToProto(&pb.Account{}, &accountInput).(*pb.Account)

	retorno, err := clientGRPC.CreateAccount(ctx, protoAccount)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	accountRetorno := common.ProtoToModel(&models.CommonResponse{}, retorno).(*models.CommonResponse)

	return accountRetorno, nil
}

func UpdateAccount(ctx context.Context, id string, accountInput models.Account) (*models.CommonResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCACCOUNT"))
	clientGRPC := pb.NewZapAccountServiceClient(conn)

	accountInput.ID = id
	var protoAccount *pb.Account = common.ModelToProto(&pb.Account{}, &accountInput).(*pb.Account)

	accountProto, err := clientGRPC.UpdateAccount(ctx, protoAccount)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	accountRetorno := common.ProtoToModel(&models.CommonResponse{}, accountProto).(*models.CommonResponse)

	return accountRetorno, nil
}

func DeleteAccount(ctx context.Context, id string) (*models.CommonResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCACCOUNT"))
	clientGRPC := pb.NewZapAccountServiceClient(conn)

	protoInputId := &pb.InputId{ID: id}

	respost, err := clientGRPC.DeleteAccount(ctx, protoInputId)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	return &models.CommonResponse{Message: respost.Message}, nil
}

func GetAccount(ctx context.Context, accountInput models.Account) (*models.Account, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCACCOUNT"))
	clientGRPC := pb.NewZapAccountServiceClient(conn)

	var params *pb.Account = common.ModelToProto(&pb.Account{}, &accountInput).(*pb.Account)

	protoAccount, err := clientGRPC.GetAccount(ctx, params)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	modelAccount := common.ProtoToModel(&models.Account{}, protoAccount).(*models.Account)

	return modelAccount, nil
}

func PagedAccount(ctx context.Context, inputPaged models.InputPaged) (*models.AccountPaged, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCACCOUNT"))
	clientGRPC := pb.NewZapAccountServiceClient(conn)

	offset := inputPaged.Offset
	limit := inputPaged.Limit

	retorno, err := clientGRPC.PagedAccount(ctx, &pb.InputPaged{Limit: int64(limit), Offset: int64(offset), Filters: inputPaged.Filters, OrderBy: inputPaged.OrderBy})
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	items := []models.Account{}
	for _, element := range retorno.Items {
		var accountModel *models.Account = common.ProtoToModel(&models.Account{}, element).(*models.Account)
		items = append(items, *accountModel)
	}

	return &models.AccountPaged{Size: int(retorno.Size), Offset: offset, Limit: limit, Items: items}, nil
}

func ChangePassword(ctx context.Context, id string, password string, oldPassword string) (*models.CommonResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCACCOUNT"))
	clientGRPC := pb.NewZapAccountServiceClient(conn)

	protoChangePassword := &pb.InputChangePassword{ID: id, Password: password, OldPassword: oldPassword}

	retorno, err := clientGRPC.ChangePassword(ctx, protoChangePassword)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	accountRetorno := common.ProtoToModel(&models.CommonResponse{}, retorno).(*models.CommonResponse)

	return accountRetorno, nil
}
