package resolvers

import (
	"context"
	"os"

	common "gitlab.com/danieldefante/zap-common"
	"gitlab.com/diogopazacvel/zap-auth-common-model/models"
	pb "gitlab.com/diogopazacvel/zap-auth-common-model/services"
)

func CreateRolePermission(ctx context.Context, roleId string, permissionId string) (*models.CommonResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	protoRolePermission := &pb.RolePermission{RoleID: roleId, PermissionID: permissionId}

	retorno, err := clientGRPC.CreateRolePermission(ctx, protoRolePermission)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	permissionRetorno := common.ProtoToModel(&models.CommonResponse{}, retorno).(*models.CommonResponse)

	return permissionRetorno, nil
}

func DeleteRolePermission(ctx context.Context, roleId string, permissionId string) (*models.CommonResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	protoRolePermission := &pb.RolePermission{RoleID: roleId, PermissionID: permissionId}

	respost, err := clientGRPC.DeleteRolePermission(ctx, protoRolePermission)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	return &models.CommonResponse{Message: respost.Message}, nil
}

func GetAllBoundRolePermission(ctx context.Context, moduleId string, roleId string) ([]*models.PermissionVO, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	protoInputBoundPermission := &pb.InputBoundPermission{ID: roleId, ModuleID: moduleId}

	permissionVOListProto, err := clientGRPC.GetAllBoundRolePermission(ctx, protoInputBoundPermission)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	items := []*models.PermissionVO{}
	for _, element := range permissionVOListProto.Items {
		var permissionVOModel *models.PermissionVO = common.ProtoToModel(&models.PermissionVO{}, element).(*models.PermissionVO)
		items = append(items, permissionVOModel)
	}

	return items, nil
}
