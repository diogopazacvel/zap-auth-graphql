package resolvers

import (
	"context"
	"os"

	common "gitlab.com/danieldefante/zap-common"
	"gitlab.com/diogopazacvel/zap-auth-common-model/models"
	pb "gitlab.com/diogopazacvel/zap-auth-common-model/services"
)

func CreateAccountPermission(ctx context.Context, accountId string, permissionId string) (*models.CommonResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	protoAccountPermission := &pb.AccountPermission{AccountID: accountId, PermissionID: permissionId}

	retorno, err := clientGRPC.CreateAccountPermission(ctx, protoAccountPermission)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	permissionRetorno := common.ProtoToModel(&models.CommonResponse{}, retorno).(*models.CommonResponse)

	return permissionRetorno, nil
}

func DeleteAccountPermission(ctx context.Context, accountId string, permissionId string) (*models.CommonResponse, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	protoAccountPermission := &pb.AccountPermission{AccountID: accountId, PermissionID: permissionId}

	respost, err := clientGRPC.DeleteAccountPermission(ctx, protoAccountPermission)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	return &models.CommonResponse{Message: respost.Message}, nil
}

func GetAllBoundAccountPermission(ctx context.Context, moduleId string, accountId string) ([]*models.PermissionVO, error) {
	conn := common.CreateConn(os.Getenv("HOSTGRPCAUTH"))
	clientGRPC := pb.NewZapAuthServiceClient(conn)

	protoInputBoundPermission := &pb.InputBoundPermission{ID: accountId, ModuleID: moduleId}

	permissionVOListProto, err := clientGRPC.GetAllBoundAccountPermission(ctx, protoInputBoundPermission)
	common.CloseConn(conn)
	if err != nil {
		return nil, err
	}

	items := []*models.PermissionVO{}
	for _, element := range permissionVOListProto.Items {
		var permissionVOModel *models.PermissionVO = common.ProtoToModel(&models.PermissionVO{}, element).(*models.PermissionVO)
		items = append(items, permissionVOModel)
	}

	return items, nil
}
