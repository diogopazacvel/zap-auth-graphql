package zapauthgraphql

import (
	"context"

	"gitlab.com/diogopazacvel/zap-auth-common-model/models"

	"zapAuthGraphql/resolvers"
)

// THIS CODE IS A STARTING POINT ONLY. IT WILL NOT BE UPDATED WITH SCHEMA CHANGES.

type Resolver struct{}

func (r *Resolver) Mutation() MutationResolver {
	return &mutationResolver{r}
}
func (r *Resolver) Query() QueryResolver {
	return &queryResolver{r}
}

type mutationResolver struct{ *Resolver }

func (r *mutationResolver) CreateApplication(ctx context.Context, applicationInput models.Application) (*models.CommonResponse, error) {
	return resolvers.CreateApplication(ctx, applicationInput)
}

func (r *mutationResolver) UpdateApplication(ctx context.Context, id string, applicationInput models.Application) (*models.CommonResponse, error) {
	return resolvers.UpdateApplication(ctx, id, applicationInput)
}

func (r *mutationResolver) DeleteApplication(ctx context.Context, id string) (*models.CommonResponse, error) {
	return resolvers.DeleteApplication(ctx, id)
}

func (r *mutationResolver) CreateModule(ctx context.Context, moduleInput models.Module) (*models.CommonResponse, error) {
	return resolvers.CreateModule(ctx, moduleInput)
}

func (r *mutationResolver) UpdateModule(ctx context.Context, id string, moduleInput models.Module) (*models.CommonResponse, error) {
	return resolvers.UpdateModule(ctx, id, moduleInput)
}

func (r *mutationResolver) DeleteModule(ctx context.Context, id string) (*models.CommonResponse, error) {
	return resolvers.DeleteModule(ctx, id)
}

func (r *mutationResolver) CreatePermission(ctx context.Context, permissionInput models.Permission) (*models.CommonResponse, error) {
	return resolvers.CreatePermission(ctx, permissionInput)
}

func (r *mutationResolver) UpdatePermission(ctx context.Context, id string, permissionInput models.Permission) (*models.CommonResponse, error) {
	return resolvers.UpdatePermission(ctx, id, permissionInput)
}

func (r *mutationResolver) DeletePermission(ctx context.Context, id string) (*models.CommonResponse, error) {
	return resolvers.DeletePermission(ctx, id)
}

func (r *mutationResolver) CreateRole(ctx context.Context, roleInput models.Role) (*models.CommonResponse, error) {
	return resolvers.CreateRole(ctx, roleInput)
}

func (r *mutationResolver) UpdateRole(ctx context.Context, id string, roleInput models.Role) (*models.CommonResponse, error) {
	return resolvers.UpdateRole(ctx, id, roleInput)
}

func (r *mutationResolver) DeleteRole(ctx context.Context, id string) (*models.CommonResponse, error) {
	return resolvers.DeleteRole(ctx, id)
}

func (r *mutationResolver) CreateAccount(ctx context.Context, accountInput models.Account) (*models.CommonResponse, error) {
	return resolvers.CreateAccount(ctx, accountInput)
}

func (r *mutationResolver) UpdateAccount(ctx context.Context, id string, accountInput models.Account) (*models.CommonResponse, error) {
	return resolvers.UpdateAccount(ctx, id, accountInput)
}

func (r *mutationResolver) DeleteAccount(ctx context.Context, id string) (*models.CommonResponse, error) {
	return resolvers.DeleteAccount(ctx, id)
}

func (r *mutationResolver) CreateRolePermission(ctx context.Context, roleId string, permissionId string) (*models.CommonResponse, error) {
	return resolvers.CreateRolePermission(ctx, roleId, permissionId)
}

func (r *mutationResolver) DeleteRolePermission(ctx context.Context, roleId string, permissionId string) (*models.CommonResponse, error) {
	return resolvers.DeleteRolePermission(ctx, roleId, permissionId)
}

func (r *mutationResolver) CreateAccountPermission(ctx context.Context, accountId string, permissionId string) (*models.CommonResponse, error) {
	return resolvers.CreateAccountPermission(ctx, accountId, permissionId)
}

func (r *mutationResolver) DeleteAccountPermission(ctx context.Context, accountId string, permissionId string) (*models.CommonResponse, error) {
	return resolvers.DeleteAccountPermission(ctx, accountId, permissionId)
}

func (r *mutationResolver) CreateAccountRole(ctx context.Context, accountId string, roleId string) (*models.CommonResponse, error) {
	return resolvers.CreateAccountRole(ctx, accountId, roleId)
}

func (r *mutationResolver) DeleteAccountRole(ctx context.Context, accountId string, roleId string) (*models.CommonResponse, error) {
	return resolvers.DeleteAccountRole(ctx, accountId, roleId)
}

func (r *mutationResolver) ChangePassword(ctx context.Context, id string, password string, oldPassword string) (*models.CommonResponse, error) {
	return resolvers.ChangePassword(ctx, id, password, oldPassword)
}

func (r *mutationResolver) RenewToken(ctx context.Context, token string) (*models.LoginResponse, error) {
	return resolvers.RenewToken(ctx, token)
}

func (r *mutationResolver) RecoverPassword(ctx context.Context, email string) (*models.CommonResponse, error) {
	return resolvers.RecoverPassword(ctx, email)
}

type queryResolver struct{ *Resolver }

func (r *queryResolver) GetApplication(ctx context.Context, applicationInput models.Application) (*models.Application, error) {
	return resolvers.GetApplication(ctx, applicationInput)
}

func (r *queryResolver) PagedApplication(ctx context.Context, inputPaged models.InputPaged) (*models.ApplicationPaged, error) {
	return resolvers.PagedApplication(ctx, inputPaged)
}

func (r *queryResolver) GetModule(ctx context.Context, moduleInput models.Module) (*models.Module, error) {
	return resolvers.GetModule(ctx, moduleInput)
}

func (r *queryResolver) PagedModule(ctx context.Context, applicationId string, inputPaged models.InputPaged) (*models.ModulePaged, error) {
	return resolvers.PagedModule(ctx, applicationId, inputPaged)
}

func (r *queryResolver) GetAllModule(ctx context.Context, applicationId string) ([]*models.Module, error) {
	return resolvers.GetAllModule(ctx, applicationId)
}

func (r *queryResolver) GetPermission(ctx context.Context, permissionInput models.Permission) (*models.Permission, error) {
	return resolvers.GetPermission(ctx, permissionInput)
}

func (r *queryResolver) PagedPermission(ctx context.Context, moduleId string, inputPaged models.InputPaged) (*models.PermissionPaged, error) {
	return resolvers.PagedPermission(ctx, moduleId, inputPaged)
}

func (r *queryResolver) GetRole(ctx context.Context, roleInput models.Role) (*models.Role, error) {
	return resolvers.GetRole(ctx, roleInput)
}

func (r *queryResolver) PagedRole(ctx context.Context, applicationId string, inputPaged models.InputPaged) (*models.RolePaged, error) {
	return resolvers.PagedRole(ctx, applicationId, inputPaged)
}

func (r *queryResolver) GetAccount(ctx context.Context, accountInput models.Account) (*models.Account, error) {
	return resolvers.GetAccount(ctx, accountInput)
}

func (r *queryResolver) PagedAccount(ctx context.Context, inputPaged models.InputPaged) (*models.AccountPaged, error) {
	return resolvers.PagedAccount(ctx, inputPaged)
}

func (r *queryResolver) GetAllBoundRolePermission(ctx context.Context, moduleId string, roleId string) ([]*models.PermissionVO, error) {
	return resolvers.GetAllBoundRolePermission(ctx, moduleId, roleId)
}

func (r *queryResolver) GetAllBoundAccountPermission(ctx context.Context, moduleId string, accountId string) ([]*models.PermissionVO, error) {
	return resolvers.GetAllBoundAccountPermission(ctx, moduleId, accountId)
}

func (r *queryResolver) GetAllAccountRoleByIDAccount(ctx context.Context, applicationId string, accountId string) ([]*models.AccountRoleVO, error) {
	return resolvers.GetAllAccountRoleByIDAccount(ctx, applicationId, accountId)
}
