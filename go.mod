module zapAuthGraphql

go 1.12

require (
	github.com/99designs/gqlgen v0.9.3
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fatih/structs v1.1.0 // indirect
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/mitchellh/mapstructure v0.0.0-20180203102830-a4e142e9c047
	github.com/rs/cors v1.7.0
	github.com/vektah/gqlparser v1.1.2
	gitlab.com/danieldefante/zap-common v0.0.0-20191015133218-d10358913f07
	gitlab.com/diogopazacvel/zap-auth-common-model v0.0.0-20191014172321-5e1193177aa0
	google.golang.org/grpc v1.23.0
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.29.1 // indirect
)
